# Hyperion config files

## Making changes

Changes need to pass ci, create a pull request e.g.

``` bash
git push origin master:foobar
```
Then ctrl+click the link in your terminal

``` bash
remote: 
remote: To create a merge request for foobar, visit:
remote:   https://gitlab.ub.uni-bielefeld.de/CLF/Robocup/hyperion_configs/-/merge_requests/new?merge_request%5Bsource_branch%5D=foobar
remote: 
```

Then create the merge request and select `Automerge after pipeline succeeded` or merge manually.

Afterwards rebase your master branch
``` bash
git pull --rebase
```

# Local Build

Setup up your prefix and source ros
``` bash
sr
```

Create build folder:
``` bash
mkdir build
cd build
```

Build hyperion

``` bash
cmake ..
make 
```

Source the build

``` bash
source devel/setup.bash
```

Launch Hyperion

``` bash
hyperion_<env_name>.sh
```

## Adding new files

You need to rerun cmake to be aware of new files
e.g. 
```
cmake ..
catkin --force-cmake
colcon --cmake-force-configure
```
depending on the build tool or by touching CMakeLists `touch CMakeLists.txt`