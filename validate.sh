rm -rf build
mkdir -p build
cd build
cmake ..
make
rm -f outfile errorfile log.out
for file in devel/share/robocup_hyperion_configs/systems/demo/*
do
    echo "validating demo file: `basename $file`"
    hyperion validate -F "$file" 2>errorfile >outfile 
    if  ! [ -s errorfile ] || [ "$(cat errorfile)" == ExitStatus.FINE ]; then
        echo ok
        break
    else
        echo error
        echo -e "Failed to validate '`basename $file`':\n" >> log.out
        cat outfile >> log.out
        cat errorfile >> log.out
    fi
done 

if [ -s log.out ]; then
    echo -e "Validation failed!" 
    exit 1
else
    exit 0
fi