###############################################################################
# Default Paths

#Map/World paths
export PATH_TO_MAPS="${prefix}/share/robocup_data/maps"
export PATH_TO_MAPS_CLF="${prefix}/share/tiago_clf_nav/maps/real"
export PATH_TO_MUJOCO_SCENES="${prefix}/share/tobi_sim/scenes"

# ECWM
export PATH_TO_MAPPR_WORLDS="${prefix}/share/tiago_clf_launch/config/ecwm"
export PATH_TO_ECWM_CUPRO="${prefix}/share/ecwm_cupro"
export PATH_TO_ECWM_DATA="${prefix}/share/ecwm_data"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix}/share/SpeechRec/psConfig"

# Path to logging config
export PATH_TO_BONSAI_LOGGING="${prefix}/opt/bonsai_robocup_tasks/etc"

# Path to bonsai configs
export PATH_TO_BONSAI_ROBOCUP_CONFIG="${prefix}/opt/bonsai_robocup_addons/etc/bonsai_configs"
export PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG="${prefix}/opt/bonsai_robocup_tasks/etc/bonsai_configs"
export PATH_TO_BONSAI_TIAGO_CONFIG="${prefix}/opt/bonsai_tiago_addons/etc/bonsai_configs"

# Path to scxml locations
export PATH_TO_BONSAI_CORE_SCXML="${prefix}/opt/bonsai-scxml_engine/etc/behaviors"
export PATH_TO_BONSAI_ROBOCUPTASKS_SCXML="${prefix}/opt/bonsai_robocup_tasks/etc/state_machines"
export PATH_TO_BONSAI_ROBOCUPTASKS_EXERCISE="${challenge_prefix}/opt/bonsai_robocup_tasks/etc/state_machines"
export PATH_TO_BONSAI_TIAGO_SCXML="${prefix}/opt/bonsai_tiago_addons/etc/state_machines"

# Create mapping variable, used by bonsai to resolve "src=" attributes in scxml files
export BONSAI_MAPPING="ROBOCUP=${PATH_TO_BONSAI_ROBOCUPTASKS_SCXML} SCXML=${PATH_TO_BONSAI_CORE_SCXML} EXERCISE=${PATH_TO_BONSAI_ROBOCUPTASKS_EXERCISE} TIAGO=${PATH_TO_BONSAI_TIAGO_SCXML}"

# YOLOX
export PATH_TO_YOLOX_MODELS="${prefix}/share/yolox_models"

# RASA NLU
export PATH_TO_RASA_MODELS="${HOME}/.rasa_models"
export PATH_TO_RASA_CONFIGS="${prefix}/../nightly-speech/share/rasa_models/config"

export PATH_TO_WHISPER_MODELS="${prefix}/../nightly-speech/share/whisper-models"

# /Default Paths
###############################################################################
