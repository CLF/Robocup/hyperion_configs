###############################################################################
# sim
export SIMMODE="true"
export laptop=${basepc}
export robot=${basepc}
export compute=${basepc}

export OPENBLAS_NUM_THREADS=1
export GOTO_NUM_THREADS=1
export OMP_NUM_THREADS=1

# Camera
## Mujoco "Realsense"
export RGB_CAM_TOPIC="/camera/color/image_raw"
export DEPTH_CAM_TOPIC_PREFIX="/camera/depth"
export DEPTH_CAM_TOPIC="$DEPTH_CAM_TOPIC_PREFIX/image_raw"
export DEPTH_CAM_INFO_TOPIC="$DEPTH_CAM_TOPIC_PREFIX/camera_info"

# Microphone
#export alsa_device=hyperx_mono
export alsa_device=default

#### ---------------
# Configs
#### ---------------

# Default Simulation map/world
export NAVIGATION_MAP="${PATH_TO_MAPS_CLF}/../sim/kassel_arena.yaml"

# Mujoco
export MUJOCO_SCENE="$PATH_TO_MUJOCO_SCENES/tiago_kassel_arena.xml.xacro"

# Keyboard movement for actors and our robot
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
export ROBOT_VEL_TOPIC="/key_vel"

# ECWM World
export ECWM_WORLD_FILE="${PATH_TO_ECWM_DATA}/worlds/sim_kassel_arena.yaml"

# /sim
###############################################################################

export setup_suffix=$(echo $0 | cut -d "/" -f3-)

###############################################################################
# yolox volume
export yolox_prefix=${prefix}
alias source_yolox="source ${yolox_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# openpose volume
export openpose_prefix=${prefix}/../openpose
alias source_openpose="source ${openpose_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# person volume
export person_prefix=${prefix}/../nightly-person
alias source_person="source ${person_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# speech volume
export speech_prefix=${prefix}/../nightly-speech
alias source_speech="source ${speech_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# mujoco volume
export mujoco_prefix=${prefix}/../nightly-mujoco
alias source_mujoco="source ${mujoco_prefix}/setup.${setup_suffix}"
###############################################################################