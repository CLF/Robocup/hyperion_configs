# Nav

export MOVEBASE_CONFIG="base"

# Rviz
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

# Bonsai 
export BONSAI_LOGGING_CONFIG="${PATH_TO_BONSAI_LOGGING}/skillsLogging.properties"
export PATH_TO_BONSAI_CONFIG="${PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG}/DefaultTiagoConfig.xml"
export PATH_TO_BONSAI_TASK="${PATH_TO_BONSAI_ROBOCUPTASKS_SCXML}/"

# YOLOX YCBV-21 Objects
export OBJECT_MODEL="${PATH_TO_YOLOX_MODELS}/models/clf_objects/best_ckpt.pth"
export YOLOX_EXP="${PATH_TO_YOLOX_MODELS}/config/exp/clf_objects.py"
export OBJECT_ECWM_TYPES="${PATH_TO_YOLOX_MODELS}/config/clf_objects_models.yaml"


######
# ECWM World
#export ECWM_WORLD_FILE="${PATH_TO_ECWM_DATA}/worlds/clf_comfy.yaml" -set in "tiago" or "sim"
export ECWM_CONFIG_FILE="${PATH_TO_ECWM_DATA}/config/robocup-default.yaml"
export ECWM_PLUGIN_FILE="${PATH_TO_ECWM_DATA}/config/plugin/robocup-default.yaml"
export ECWM_MODELS="${PATH_TO_ECWM_DATA}/models:${prefix}/share/tiago_clf_launch/config/ecwm/models:${PATH_TO_ECWM_CUPRO}/models"


# Tensorflow Recognition
export OBJECT_DET_PATH="${prefix}/share/robocup_data/tensorflow/detection/md_all"
export OBJECT_REC_PATH="${prefix}/share/robocup_data/tensorflow/recognition/cupro_02"

export OBJECT_DET_GRAPH="${OBJECT_DET_PATH}/frozen_inference_graph.pb"
export OBJECT_DET_LABELS="${OBJECT_DET_PATH}/labelMap.pbtxt"
export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"

export GRASPING_OBJECTS_CONFIG="${prefix}/share/storing_groceries_node/config/challenge2020.yaml"

# Tensorflow segmentation
export SEGMENTATION_CONFIGS="${prefix}/share/clf_object_recognition_config/config"
export KINECT_COLOR_XML="${SEGMENTATION_CONFIGS}/kinect-color-vga.xml"
export KINECT_DEPTH_XML="${SEGMENTATION_CONFIGS}/kinect-depth-vga.xml"
export SEGMENTER_PATH="${SEGMENTATION_CONFIGS}/segmentation_config.xml"
export ROBOT_FILTER="${SEGMENTATION_CONFIGS}/robotfilter.xml"
export OBJECT_MERGER_THRESHOLD=0.5

export alsa_device=sysdefault
export realsense_config=near

export PSA_CONFIG="${PATH_TO_PSA_CONFIG}/demos/wdr.conf"


# RSpeech - NG
export RASA_TELEMETRY_ENABLED=false
export RASA_CACHE_DIRECTORY=${HOME}/.rasa
export RASA_CONFIG="${PATH_TO_RASA_CONFIGS}/default_pipeline.yml"
export RASA_NLU_FILE="${PATH_TO_RASA_CONFIGS}/nlu/confirm.yml"
export WHISPER_MODEL="${PATH_TO_WHISPER_MODELS}/distill-medium.en.pt"
export FASTER_WHISPER_MODEL="${PATH_TO_WHISPER_MODELS}/faster-distil-whisper-medium.en"
export FASTER_WHISPER_QUANT="default"

export PIPER_MODELS="${prefix}/share/piper_models"
export NLLB_MODEL="${prefix}/share/nllb-models/nllb-200-distilled-600M"
export NLLB_MODEL_CT2="${prefix}/share/nllb-models/ct2-nllb-200-distilled-600M"

# BayesPeopleTracker

export BAYES_PEOPLE_TRACKER_CONFIG_FILE="${prefix}/share/bayes_people_tracker/config/detectors.yaml"
