###############################################################################
# ros
# ROS source alias
export setup_suffix=$(echo $0 | cut -d "/" -f3-)
alias source_ros="source ${prefix}/setup.${setup_suffix}"
alias reset_ros="unset ROS_DISTRO ROS_PACKAGE_PATH ROS_ROOT ROS_VERSION ROS_ETC_DIR ROS_PYTHON_VERSION PYTHONPATH PYTHON_CODE_BUILD_ROS_PACKAGE_PATH PYTHON_CODE_BUILD_ROSLISP_PACKAGE_DIRECTORIES"
# /ros
###############################################################################