###############################################################################
# objrec-tensorflow
export tensorflow_prefix=/vol/tiago/objrec-tensorflow

# ROS source alias
export setup_suffix=$(echo $0 | cut -d "/" -f3-)
alias source_tensorflow="source ${objrec_tensorflow_prefix}/setup.${setup_suffix}"

export TENSORFLOW_DEBUG=false
export OBJECT_CLASSIFICATION_THRESHOLD="0.4"
export OBJECT_DETECTION_THRESHOLD="0.5"

# /objrec-tensorflow 
###############################################################################