###############################################################################
# tiago
export SIMMODE="false"
export laptop=$basepc
export robot=tiago-47c
export compute=tiago-47j

export ROS_MASTER_URI=http://${robot}:11311

# Devices
export alsa_device=default

## Topics
export RGB_CAM_TOPIC="/camera/color/image_rect_color"
export DEPTH_CAM_TOPIC_PREFIX="/camera/aligned_depth_to_color"
export DEPTH_CAM_TOPIC="$DEPTH_CAM_TOPIC_PREFIX/image_raw"
export DEPTH_CAM_INFO_TOPIC="$DEPTH_CAM_TOPIC_PREFIX/camera_info"

#### ---------------
# Configs
#### ---------------
export RVIZ_CONFIG="${prefix}/share/tiago_clf_launch/config/config-kinetic.rviz"

# Default map/world
export PATH_TO_MAPS="${prefix}/share/robocup_data/maps"
export PATH_TO_MAPS_CLF="${prefix}/share/tiago_clf_nav/maps/real"
export NAVIGATION_MAP="${PATH_TO_MAPS_CLF}/clf_comfy.yaml"
export ECWM_WORLD_FILE="${PATH_TO_ECWM_DATA}/worlds/clf_comfy.yaml"

# Other
export FASTER_WHISPER_QUANT="bfloat16"

# /tiago
###############################################################################

export setup_suffix=$(echo $0 | cut -d "/" -f3-)

###############################################################################
# robot volume
export robot_prefix=/vol/tiago/robot-47
alias source_robot="source ${robot_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# yolox volume
export yolox_prefix=${prefix}/../yolox
alias source_yolox="source ${yolox_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# openpose volume
export openpose_prefix=${prefix}/../openpose
alias source_openpose="source ${openpose_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# person volume
export person_prefix=${prefix}/../nightly-person
alias source_person="source ${person_prefix}/setup.${setup_suffix}"
###############################################################################

###############################################################################
# speech volume
export speech_prefix=${prefix}/../nightly-speech
alias source_speech="source ${speech_prefix}/setup.${setup_suffix}"
###############################################################################


###############################################################################
# jp60 volumes
export jpprefix=/vol/tiago/one/jp60
alias source_jp_whisper="source ${jpprefix}/whisper/setup.${setup_suffix}"

export FASTER_WHISPER_MODEL_COMPUTE="${jpprefix}/whisper/share/whisper-models/faster-whisper-large-v3.en"
export FASTER_WHISPER_QUANT_COMPUTE="float16"

alias source_jp_translate="source ${jpprefix}/translate/setup.${setup_suffix}"
export NLLB_MODEL_COMPUTE="${jpprefix}/translate/share/nllb-models/nllb-200-distilled-600M"
export NLLB_MODEL_CT2_COMPUTE="${jpprefix}/translate/share/nllb-models/ct2-nllb-200-distilled-600M"

alias source_jp_tracker="source ${jpprefix}/tracking/setup.${setup_suffix}"
alias source_jp_llm="source ${jpprefix}/llm/setup.${setup_suffix}"
###############################################################################
